package com.example.simpleapps

import java.util.*

class BubbleSort {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val jumlah: Int
            var i = 0
            var swap: Int

            val scanner = Scanner(System.`in`)
            println("massukkan jumlah bilangan yang ingin diInputkan:")
            jumlah = scanner.nextInt()

            val array =  IntArray(jumlah)
            println("masukkan $jumlah buah bilangan")
            println("================================================")

            for (e in 0 until jumlah) {
                array[e] = scanner.nextInt();
            }

            array.forEach { print("$it ") }

            for (b in 0 until (jumlah - 1)) {
                for (c in 0 until (jumlah - b - 1)) {
                    if (array[c] > array[c + 1]) {
                        swap = array[c]
                        array[c] = array[c + 1]
                        array[c + 1] = swap
                        i++
                        println("\n")
                        print("$i. [${array[c]} - ${array[c + 1]}] >> ")
                        array.forEach { print("$it ") }
                        println("\n")
                    }
                }
            }
        }
    }
}