package com.example.simpleapps

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_reserve.setOnClickListener {
            val result = edt_input.text.toString()
            var newResult = ""
            val charArray = result.toCharArray()
            for (i in charArray.size- 1 downTo 0) {
                newResult += charArray[i]
            }
            tv_result.text = newResult
        }

        btn_undo_redo.setOnClickListener {
            edt_input.addTextChangedListener(object: TextWatcher{
                override fun afterTextChanged(s: Editable?) {

                }

                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {

                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                }

            })
        }
    }
}